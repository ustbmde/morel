package edu.ustb.sei.mde.bxcore.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.ustb.sei.mde.graph.type.TypeEdge;
import edu.ustb.sei.mde.graph.type.TypeGraph;
import edu.ustb.sei.mde.graph.type.TypeNode;
import edu.ustb.sei.mde.graph.typedGraph.ITypedGraphMerger;
import edu.ustb.sei.mde.graph.typedGraph.TypedEdge;
import edu.ustb.sei.mde.graph.typedGraph.TypedGraph;
import edu.ustb.sei.mde.graph.typedGraph.TypedNode;

class TestGraphMerger {
	
	private TypeGraph typeGraph;
	private TypedGraph typedGraph;

	@BeforeEach
	void setUp() throws Exception {
		buildTypeGraph();
		buildTypedGraph();
	}

	private void buildTypeGraph() {
		typeGraph = new TypeGraph();
		
		// add type nodes
		typeGraph.declare("A");
		typeGraph.declare("B");
		typeGraph.declare("C");
		
		// add data type nodes
		typeGraph.declare("String:java.lang.String");
		
		// add type edges
		typeGraph.declare("a2b:A->B*");
		typeGraph.declare("a2c:A->C");
		
		// add property edges
		typeGraph.declare("a2S:A->String#");
	}
	
	public void buildTypedGraph() {
		typedGraph = new TypedGraph(typeGraph);
		
		// add nodes
		typedGraph.declare(
		"a1:A;"
				+"b1:B;"
				+"b2:B;"
				+"b3:B;"
				+"c1:C;"
				+"a1-a2b->b1;"
				+"a1-a2b->b2;"
				+"a1-a2b->b3;"
				+"a1-a2c->c1;"
		);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testNodeAdditionDeletion() {
		// add b3 in branch 1
		TypedGraph branch1 = typedGraph.getCopy();
		branch1.declare("b3:B");
		
		
		// delete b2 in branch 2
		TypedGraph branch2 = typedGraph.getCopy();
		TypedNode del = branch2.getTypedNodesOf(typeGraph.getTypeNode("B"))[1];
		branch2.remove(del);
		
		TypedGraph result = ITypedGraphMerger.merger.merge(typedGraph, branch1, branch2);
		
		String str = result.printGraph();
		System.out.println(str);
		
	}
	
	@Test
	void testEdgeAdditionDeletion() {
		// add b3 in branch 1
		TypedGraph branch1 = typedGraph.getCopy();
		TypedNode b3 = new TypedNode(typeGraph.getTypeNode("B"));
		branch1.addTypedNode(b3);
		
		branch1.addTypedEdge(
				new TypedEdge(
						branch1.getTypedNodesOf(typeGraph.getTypeNode("A"))[0], 
						b3,
						typeGraph.getTypeEdge(typeGraph.getTypeNode("A"), "a2b")));
		
		// delete b2 in branch 2
		TypedGraph branch2 = typedGraph.getCopy();
		TypedNode del = branch2.getTypedNodesOf(typeGraph.getTypeNode("B"))[1];
		branch2.remove(del);
		
		TypedGraph result = ITypedGraphMerger.merger.merge(typedGraph, branch1, branch2);
		
		String str = result.printGraph();
		System.out.println(str);
		
	}
	
	@Test
	void testEdgeReordering() {
		//
		System.out.println("testEdgeReordering:");
		
		System.out.println(typedGraph.printGraph());
		
		TypeNode A = typeGraph.getTypeNode("A");
		TypeEdge A2B = typeGraph.getTypeEdge(A, "a2b");
		
		// move a->b3 to first in branch 1
		TypedGraph branch1 = typedGraph.getCopy();
		TypedNode a11 = branch1.getTypedNodesOf(A)[0];
		
//		TypedNode b4 = new TypedNode(typeGraph.getTypeNode("B"));
//		branch1.addTypedNode(b4);
//		
//		branch1.addTypedEdge(
//			new TypedEdge(branch1.getTypedNodesOf(A)[0], b4, A2B)
//		);
//		
		List<TypedEdge> a2b_edges_1 = branch1.getOutgoingEdges(a11, A2B);
		branch1.getAllTypedEdges().remove(a2b_edges_1.get(2));
		branch1.getAllTypedEdges().add(0, a2b_edges_1.get(2));

		
		// move a->b2 to first in branch 2
		TypedGraph branch2 = typedGraph.getCopy();
		TypedNode a21 = branch2.getTypedNodesOf(A)[0];
		List<TypedEdge> a2b_edges_2 = branch2.getOutgoingEdges(a21, A2B);
		Collections.swap(branch2.getAllTypedEdges(), 
				branch2.getAllTypedEdges().indexOf(a2b_edges_2.get(0)), 
				branch2.getAllTypedEdges().indexOf(a2b_edges_2.get(1)));
		
		TypedGraph result = ITypedGraphMerger.merger.merge(typedGraph, branch1, branch2);
		
		String str = result.printGraph();
		System.out.println(str);
		
	}
	
	@Test
	void testEdgeReordering2() {
		//
		System.out.println("testEdgeReordering2:");
		
		System.out.println(typedGraph.printGraph());
		
		TypeNode A = typeGraph.getTypeNode("A");
		TypeEdge A2B = typeGraph.getTypeEdge(A, "a2b");
		
		// move a->b3 to first in branch 1
		TypedGraph branch1 = typedGraph.getCopy();
		TypedNode a11 = branch1.getTypedNodesOf(A)[0];
		
		TypedNode b4 = new TypedNode(typeGraph.getTypeNode("B"));
		branch1.addTypedNode(b4);
//		
		branch1.addTypedEdge(
			new TypedEdge(branch1.getTypedNodesOf(A)[0], b4, A2B)
		);
//		
		List<TypedEdge> a2b_edges_1 = branch1.getOutgoingEdges(a11, A2B);
		branch1.getAllTypedEdges().remove(a2b_edges_1.get(2));
		branch1.getAllTypedEdges().remove(a2b_edges_1.get(3));
		
		branch1.getAllTypedEdges().add(0, a2b_edges_1.get(3));
		branch1.getAllTypedEdges().add(0, a2b_edges_1.get(2));

		
		// move a->b2 to first in branch 2
		TypedGraph branch2 = typedGraph.getCopy();
		TypedNode a21 = branch2.getTypedNodesOf(A)[0];
		List<TypedEdge> a2b_edges_2 = branch2.getOutgoingEdges(a21, A2B);
		Collections.swap(branch2.getAllTypedEdges(), 
				branch2.getAllTypedEdges().indexOf(a2b_edges_2.get(0)), 
				branch2.getAllTypedEdges().indexOf(a2b_edges_2.get(1)));
		
		TypedGraph result = ITypedGraphMerger.merger.merge(typedGraph, branch1, branch2);
		
		String str = result.printGraph();
		System.out.println(str);
		
	}

}

package edu.ustb.sei.mde.graph.typedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import edu.ustb.sei.mde.bxcore.XmuCoreUtils;
import edu.ustb.sei.mde.bxcore.exceptions.NothingReturnedException;
import edu.ustb.sei.mde.graph.type.PropertyEdge;
import edu.ustb.sei.mde.graph.type.TypeEdge;
import edu.ustb.sei.mde.graph.type.TypeNode;
import edu.ustb.sei.mde.graph.typedGraph.constraint.GraphConstraint;
import edu.ustb.sei.mde.structure.Tuple3;

public class ParallelTypedGraphMerger implements ITypedGraphMerger {
	
	protected <K,V> Map<K,V> autoMap() {
		return new HashMap<K, V>();
	}
	
	protected <K> Stream<K> autoStream(Collection<K> col) {
		return col.stream();
	}

	public TypedGraph additiveMerge(TypedGraph host, TypedGraph addition) {
		assert host.getTypeGraph() == addition.getTypeGraph();
		
		final int maxTypedNodeSize = host.getAllTypedNodes().size() + addition.getAllTypedNodes().size();
		final int maxValueNodeSize = host.getAllValueNodes().size() + addition.getAllValueNodes().size();
		final int maxTypedEdgeSize = host.getAllTypedEdges().size() + addition.getAllTypedEdges().size();
		final int maxValueEdgeSize = host.getAllValueEdges().size() + addition.getAllValueEdges().size();
		
		// make sure that the result does not have to copy array contents
		TypedGraph result = new TypedGraph(host.getTypeGraph(),maxTypedNodeSize, 0, maxTypedEdgeSize, maxValueEdgeSize);

		// use the lock when concurrent writes happen
		Lock resultLock = new ReentrantLock(); 
		
		// A map from the host typed nodes to addition typed nodes
		Map<TypedNode, TypedNode> nodeMappings = autoMap();
		
		
		// 1. handle typed nodes
		autoStream(addition.getAllTypedNodes()).forEach(n->{ // may be in parallel
			try {
				TypedNode nr = host.getElementByIndexObject(n.getIndex());
				if(nr!=n) {
					nodeMappings.put(nr, n);
				}
			} catch (NothingReturnedException e) {
				// if n is a new node, we add it to result directly
				resultLock.lock();
				result.addTypedNode(n);
				resultLock.unlock();
			}
		});
		
		autoStream(host.getAllTypedNodes()).forEach(nr->{ // may be in parallel
			TypedNode n = nodeMappings.get(nr);
			if(n==null) { // nr is not replaced
				resultLock.lock();
				result.addTypedNode(nr);
				resultLock.unlock();
			} else { // nr is replaced with n
				resultLock.lock();
				result.addTypedNode(n);
				resultLock.unlock();
			}
		});
		
		// 2. handle value nodes
		// we need this set to avoid duplicated value nodes.
		final Set<ValueNode> allValueNodes = new HashSet<>(maxValueNodeSize);
		allValueNodes.addAll(host.getAllValueNodes());
		allValueNodes.addAll(addition.getAllValueNodes());
		// currently it is safe to use addAll, because value nodes are not indexed
		result.getAllValueNodes().addAll(allValueNodes);
		
		
		// 3. handle typed edges
		Map<TypedEdge, TypedEdge> typeEdgeMappings = autoMap();
		List<TypedEdge> addedTypeEdges = new ArrayList<>(addition.getAllTypedEdges().size());
		
		addition.getAllTypedEdges().forEach(e->{ // to ensure the order, cannot be performed in parallel now
			try {
				TypedEdge er = host.getElementByIndexObject(e.getIndex());
				if(er!=e) {
					typeEdgeMappings.put(er, e);
				}
			} catch (NothingReturnedException ex) {
				addedTypeEdges.add(e);
			}
		});
		
		host.getAllTypedEdges().forEach(er->{
			TypedEdge e = typeEdgeMappings.get(er);
			if(e==null) {
				// check if the source and the target are changed
				TypedNode s = nodeMappings.getOrDefault(er.getSource(), er.getSource());
				TypedNode t = nodeMappings.getOrDefault(er.getTarget(), er.getTarget());
				if(s!=er.getSource() || t!=er.getTarget()) {
					TypedEdge newE = new TypedEdge(s, t, er.getType());
					newE.mergeIndex(er);
					result.addTypedEdge(newE);
				} else 
					result.addTypedEdge(er);
			} else {
				// er is replaced with e
				result.addTypedEdge(e);
			}
		});
		
		addedTypeEdges.forEach(e->result.addTypedEdge(e));
		
		// 4. handle value edges
		Map<ValueEdge, ValueEdge> valueEdgeMappings = autoMap();
		List<ValueEdge> addedValueEdges = new ArrayList<>(addition.getAllValueEdges().size());
		
		addition.getAllValueEdges().forEach(e->{ // to ensure the order, cannot be performed in parallel now
			try {
				ValueEdge er = host.getElementByIndexObject(e.getIndex());
				if(er!=e) {
					valueEdgeMappings.put(er, e);
				}
			} catch (NothingReturnedException ex) {
				addedValueEdges.add(e);
			}
		});
		
		host.getAllValueEdges().forEach(er->{
			ValueEdge e = valueEdgeMappings.get(er);
			if(e==null) {
				// check if the source and the target are changed
				TypedNode s = nodeMappings.getOrDefault(er.getSource(), er.getSource());
				if(s!=er.getSource()) {
					ValueEdge newE = new ValueEdge(s, er.getTarget(), er.getType());
					newE.mergeIndex(er);
					result.addValueEdge(newE);
				} else 
					result.addValueEdge(er);
			} else {
				// er is replaced with e
				result.addValueEdge(e);
			}
		});
		
		addedValueEdges.forEach(e->result.addValueEdge(e));
		
		
		// 5. handle aux
		result.getOrder().merge(result.getOrder());
		result.setConstraint(GraphConstraint.and(host.getConstraint(), addition.getConstraint()));
		// check 

		return result;
	}
	
	
	
	
	public TypedGraph merge(TypedGraph base, TypedGraph... interSources) throws NothingReturnedException {
		int extraSize = interSources.length * 16;
		
		TypedGraph result = new TypedGraph(base.getTypeGraph(),
				base.getAllTypedNodes().size() + extraSize, 
				base.getAllValueNodes().size() + extraSize, 
				base.getAllTypedEdges().size() + extraSize,
				base.getAllValueEdges().size() + extraSize);
		// use the lock when concurrent writes happen
		Lock resultLock = new ReentrantLock(); 
				
		
		// 1. compute replaced and removed nodes
		
		Set<TypedNode> removedNodes = new HashSet<>();
		Map<TypedNode, TypedNode> replacedNodes = autoMap();
		
		autoStream(base.getAllTypedNodes()).forEach(baseNode->{
			TypeNode[] nodeImages = new TypeNode[interSources.length];
			for(int i=0;i<interSources.length;i++) {
				nodeImages[i] = TypedGraph.computeImage(baseNode, base, interSources[i]);
			}
			
			try {
				TypeNode finalType = TypedGraph.computeType(nodeImages, base.getTypeGraph());
				
				if(finalType==TypeNode.NULL_TYPE) {
					removedNodes.add(baseNode);
				} else {
					if(finalType==TypeNode.ANY_TYPE) {
						// no need to create new node
						resultLock.lock();
						result.addTypedNode(baseNode);
						resultLock.unlock();
					} else {
						// it is also possible that sub nodes are equal to baseNode
						TypedNode n = null;
						
						// if the final type is different from base type, we create a new node
						if(finalType!=baseNode.getType()) {
							n = null;
						} else {
							n = baseNode; // default value
						}
						
						for(TypedGraph image : interSources) {
							IndexableElement branchNode = image.getElementByIndexObject(baseNode.getIndex());
							if(branchNode!=n) { // if branchNode!=n, especially branchNode!=baseNode
								// since branchNode!=null, the then branch must be entered once if n==null
								// if the then branch is not performed, it means all branchNodes are baseNode
								if(n==null || n==baseNode) { // if n==null || n==baseNode, we create a new node
									n = new TypedNode();
									n.setType(finalType);
								}
								n.mergeIndex(branchNode);								
							}
						}
						
						resultLock.lock();
						result.addTypedNode(n);
						resultLock.unlock();
						if(n!=baseNode) 
							replacedNodes.put(baseNode, n);						
					}
				}
				
			} catch (NothingReturnedException e) {
				throw e;
			}
		});
		
		
		// 2. add value nodes
		Set<ValueNode> allValueNodes = new HashSet<>();
		allValueNodes.addAll(base.getAllValueNodes());
		for(TypedGraph image : interSources) {
			allValueNodes.addAll(image.getAllValueNodes());
		}
		result.getAllValueNodes().addAll(allValueNodes);
		
		// do grouping
		base.grouping();
		
		// 3. handle typed edges
		TypedEdge[] typedEdgeImages = new TypedEdge[interSources.length];
		
		Set<List<TypedEdge>> typedEdgeGroups = base.typedEdgeGroups;
		
		autoStream(typedEdgeGroups).forEach(group->{
			for(TypedEdge baseEdge : group) {
				for(int i=0;i<interSources.length;i++) {
					typedEdgeImages[i] = TypedGraph.computeImage(baseEdge, base, interSources[i]);
				}
				try {
					Tuple3<TypedNode, TypedNode, TypeEdge> finalEdgeInfo = TypedGraph.computeEdge(baseEdge, typedEdgeImages);
					if(finalEdgeInfo!=null) {
						TypedNode source = replacedNodes.getOrDefault(baseEdge.getSource(), removedNodes.contains(baseEdge.getSource())? null :baseEdge.getSource());
						TypedNode target = replacedNodes.getOrDefault(baseEdge.getTarget(), removedNodes.contains(baseEdge.getTarget())? null :baseEdge.getTarget());
						
						if(source==null || target==null) {
							// finalEdgeInfo is valid but the source/target is removed
							throw new NothingReturnedException();
						} else {
							// first check if baseEdge can be reused by iterating typedEdgeImages
							if(Stream.of(typedEdgeImages).allMatch(e->e==baseEdge)) {
								resultLock.lock();
								result.addTypedEdge(baseEdge);
								resultLock.unlock();
							} else {
								TypedEdge edge = new TypedEdge();
								edge.setSource(source);
								edge.setTarget(target);
								edge.setType(finalEdgeInfo.third);
								edge.appendIndex(baseEdge.getIndex());
								
								for(TypedEdge image : typedEdgeImages) {
									edge.mergeIndex(image);
								}
								
								resultLock.lock();
								result.addTypedEdge(edge);
								resultLock.unlock();
							}
						}
					}
				} catch (NothingReturnedException e) {
					throw e;
				}
			}
		});
		
		
		// 4. handle value edges
		ValueEdge[] valueEdgeImages = new ValueEdge[interSources.length];
		Set<List<ValueEdge>> valueEdgeGroups = base.valueEdgeGroups;
		
		autoStream(valueEdgeGroups).forEach(group->{
			for(ValueEdge baseEdge : group) {
				for(int i=0;i<interSources.length;i++) {
					valueEdgeImages[i] = TypedGraph.computeImage(baseEdge, base, interSources[i]);
				}
				try {
					Tuple3<TypedNode, ValueNode, PropertyEdge> finalEdgeInfo = TypedGraph.computeEdge(baseEdge, valueEdgeImages);
					if(finalEdgeInfo!=null) {
						TypedNode source = replacedNodes.getOrDefault(baseEdge.getSource(), removedNodes.contains(baseEdge.getSource())? null :baseEdge.getSource());
						ValueNode target = finalEdgeInfo.second;
						
						if(source==null) {
							throw new NothingReturnedException();
						} else {
							if(Stream.of(valueEdgeImages).allMatch(e->e==baseEdge)) {
								resultLock.lock();
								result.addValueEdge(baseEdge);
								resultLock.unlock();
							} else {
								ValueEdge edge = new ValueEdge();
								edge.setSource(source);
								edge.setTarget(target);
								edge.setType(finalEdgeInfo.third);
								edge.appendIndex(baseEdge.getIndex());
								
								for(ValueEdge image : valueEdgeImages) {
									edge.mergeIndex(image);
								}
								resultLock.lock();
								result.addValueEdge(edge);
								resultLock.unlock();
							}
						}
					}
				} catch (NothingReturnedException e) {
					throw e;
				}
			}
		});
		
		
		
		// 5. handle extra
		autoStream(Arrays.asList(interSources)).forEach(image->{
//			for(TypedGraph image : interSources) {
			for(TypedNode n : image.getAllTypedNodes()) {
				try {
					base.getElementByIndexObject(n.getIndex());
				} catch (NothingReturnedException e) {
					TypedNode rn = null;
					resultLock.lock();
					try {
						rn = result.getElementByIndexObject(n.getIndex()); // check if being added by another source
					} catch (NothingReturnedException e1) {
						result.addTypedNode(n); // n is fresh new
						rn = null;
					} finally {
						if(rn!=null) {
							TypeNode lt = rn.getType();
							TypeNode rt = n.getType();
							TypeNode ct = base.getTypeGraph().computeSubtype(lt, rt);
							if(ct==TypeNode.NULL_TYPE) {
								// incompatible
								resultLock.unlock(); // release before exception
								throw new NothingReturnedException();
							} else {
								// in rare cases, rn.type may be changed by another thread inconsistently
								rn.setType(ct);
								result.addIndex(n, rn); // at this point, no exception will be thrown
							}
						}
					}
					resultLock.unlock();
				}
			}
			
			for(TypedEdge e : image.getAllTypedEdges()) {
				try {
					base.getElementByIndexObject(e.getIndex());
				} catch (Exception ex) {
					TypedEdge re = null;
					resultLock.lock();
					try {
						re = result.getElementByIndexObject(e.getIndex());
					} catch (Exception ex2) {
						TypedNode source = result.getElementByIndexObject(e.getSource().getIndex());
						TypedNode target = result.getElementByIndexObject(e.getTarget().getIndex());
						if(e.getSource()!=source || e.getTarget()!=target) {
							TypedEdge ne = new TypedEdge();
							ne.setSource(source);
							ne.setTarget(target);
							ne.setType(e.getType());
							ne.mergeIndex(e);
							
							result.addTypedEdge(ne);
						} else {
							result.addTypedEdge(e);
						}
						re = null;
					} finally {
						if(re!=null) {
							if(re.getType()!=e.getType()
									|| !re.getSource().getIndex().equals(e.getSource().getIndex())
									|| !re.getTarget().getIndex().equals(e.getTarget().getIndex())) {
								resultLock.unlock();
								throw new NothingReturnedException();
							} else {
								result.addIndex(e, re);
							}
						}
					}
					resultLock.unlock();
				}
			}
			
			for(ValueEdge e : image.getAllValueEdges()) {
				try {
					base.getElementByIndexObject(e.getIndex());
				} catch (Exception ex) {
					ValueEdge re = null;
					resultLock.lock();
					try {
						re = result.getElementByIndexObject(e.getIndex());
					} catch (Exception ex2) {
						TypedNode source = result.getElementByIndexObject(e.getSource().getIndex());
						if(e.getSource()!=source) {
							ValueEdge ne = new ValueEdge();
							ne.setSource(source);
							ne.setTarget(e.getTarget());
							ne.setType(e.getType());
							ne.mergeIndex(e);
							
							result.addValueEdge(ne);
						} else {
							result.addValueEdge(e);
						}
						re = null;
					} finally {
						if(re!=null) {
							if(re.getType()!=e.getType()
									|| !re.getSource().getIndex().equals(e.getSource().getIndex())
									|| re.getTarget().equals(e.getTarget())==false) {
								resultLock.unlock();
								throw new NothingReturnedException();
							} else {
								result.addIndex(e, re);
							}
						}
					}
					resultLock.unlock();
				}
			}
//			}
		});
		
		// 6. handle aux.
		
		// 1. lift order changes up to hard orders in branches
		// 2. merge hard orders into result
		// 3. sort with cache
		OrderInformation[] orders = new OrderInformation[interSources.length];
		for(int i=0;i<interSources.length;i++)
			orders[i] = interSources[i].getOrder();
		result.getOrder().merge(orders);
		
		result.grouping();
		for(TypedGraph branch : interSources) branch.grouping();
		
		enforceOrder(result, base, interSources, g->g.typedEdgeGroups, d->d.getAllTypedEdges());
		
		enforceOrder(result, base, interSources, g->g.valueEdgeGroups, d->d.getAllValueEdges());
		
		List<GraphConstraint> cons = new ArrayList<>();
		cons.add(base.getConstraint());
		for(TypedGraph g : interSources) {
			cons.add(g.getConstraint());
		}
		result.setConstraint(GraphConstraint.and(cons));
		// check
		
		return result;
	}
	
	
	private <K extends IndexableElement> void enforceOrder(TypedGraph result, TypedGraph base, TypedGraph[] interSources, Function<TypedGraph, Set<List<K>>> groups, Function<TypedGraph, List<K>> data) {
		LocalOrder[] branchOrders = new LocalOrder[interSources.length];
		BaseLocalOrder baseOrder = buildLocalOrder(base, interSources, branchOrders, groups);
		
		
		autoStream(groups.apply(result)).forEach(list->{ 
			try {
				OrderKind martix[][] = new OrderKind[list.size()][list.size()];
				for(int f=0;f<list.size();f++) {
					K fo = list.get(f);
					for(int t=f+1;t<list.size();t++) {
						K to = list.get(t);
						OrderKind order =  baseOrder.checkOrder(fo, to, branchOrders);
						if(order==OrderKind.HARD_GT) martix[f][t] = OrderKind.HARD_GT;
						else if(order==OrderKind.HARD_LE) martix[t][f] = OrderKind.HARD_GT;
						else if(order==OrderKind.SOFT_GT) martix[f][t] = OrderKind.SOFT_GT;
						else if(order==OrderKind.SOFT_LE) martix[t][f] = OrderKind.SOFT_GT;
					}
				}
				topologicalSort(list, martix);
			} catch (Exception e) {
				XmuCoreUtils.log(Level.SEVERE, "Sorting error! Nothing will be changed!", e);
			}
		});
		
		data.apply(result).clear();
		for(List<K> subList : groups.apply(result)) {
			data.apply(result).addAll(subList);
		}
	}
	
	private <K extends IndexableElement> void topologicalSort(List<K> list, OrderKind[][] martix) {
		Set<K> sorted = new HashSet<>();
		Set<K> inPath = new HashSet<>();
		List<K> result = new ArrayList<>(list.size());
		
		for(int ti = 0; ti < list.size(); ti++) {
			K to = list.get(ti);
			if(sorted.contains(to)) continue;
			boolean isRoot = true;
			for(int fi = 0; fi < list.size(); ti++) {
				OrderKind order = martix[fi][ti];
				if(order!=null) {
					isRoot=false;
					break;
				}
			}
			
			if(isRoot) {
				traverseFrom(ti, to, list, sorted, inPath, martix, result);
			}
		}
		list.clear();
		list.addAll(result);
	}

	private <K extends IndexableElement> void traverseFrom(int ti, K to, List<K> list, Set<K> sorted, Set<K> inPath, OrderKind[][] martix, List<K> result) {
		sorted.add(to);
		inPath.add(to);
		for(int i=0;i<list.size();i++) {
			K no = list.get(i);
			if(inPath.contains(no)) {
				throw new RuntimeException("Order conflict!");
			}
			if(sorted.contains(no) || martix[ti][i]==null) continue;
			traverseFrom(i, no, list, sorted, inPath, martix, result);
		}
		inPath.remove(to);
		result.add(to);
	}

	static <K extends IndexableElement> BaseLocalOrder buildLocalOrder(TypedGraph base, TypedGraph[] branches, LocalOrder[] branchOrders, Function<TypedGraph, Set<List<K>>> grouping) {
		
		Set<List<K>> groups = grouping.apply(base);
		 // NOTE: base = {a}, left = {b<a}, right = {a<c}, then test(b,c) will return UNK
		 BaseLocalOrder baseOrder = base.getOrder().buildLocalOrder(groups);
		 for(int i=0;i<branches.length;i++) {
				TypedGraph branch = branches[i];
				groups = grouping.apply(branch);
				branchOrders[i] = branch.getOrder().buildLocalOrder(groups, baseOrder.positionMap);
		 }
		 return baseOrder;
	}
}

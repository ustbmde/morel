package edu.ustb.sei.mde.graph.typedGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ustb.sei.mde.bxcore.exceptions.NothingReturnedException;
import edu.ustb.sei.mde.graph.type.PropertyEdge;
import edu.ustb.sei.mde.graph.type.TypeEdge;
import edu.ustb.sei.mde.graph.type.TypeNode;
import edu.ustb.sei.mde.graph.typedGraph.constraint.GraphConstraint;
import edu.ustb.sei.mde.structure.Tuple3;

public class DefaultTypedGraphMerger implements ITypedGraphMerger {

	@Override
	public TypedGraph additiveMerge(TypedGraph host, TypedGraph addition) {
		TypedGraph result = host.getCopy();
		
		addition.getAllTypedNodes().forEach(n->{
			try {
				TypedNode nr = result.getElementByIndexObject(n.getIndex());
				if(nr!=n) {
					result.replaceWith(nr,n);
				}
			} catch (NothingReturnedException e) {
				result.addTypedNode(n);
			}
		});
		
		addition.getAllValueNodes().forEach(n->{
			result.addValueNode(n);
		});
		
		addition.getAllTypedEdges().forEach(e->{
			try {
				TypedEdge er = result.getElementByIndexObject(e.getIndex());
				if(er!=e) {
					result.replaceWith(er, e);
				}
			} catch (NothingReturnedException ex) {
				result.addTypedEdge(e);
			}
		});
		
		addition.getAllValueEdges().forEach(e->{
			try {
				ValueEdge er = result.getElementByIndexObject(e.getIndex());
				if(er!=e) {
					result.replaceWith(er, e);
				}
			} catch (NothingReturnedException ex) {
				result.addValueEdge(e);
			}
		});
		
		result.getOrder().merge(addition.getOrder());
		
		result.setConstraint(GraphConstraint.and(host.getConstraint(), addition.getConstraint()));
		// check 
		
		return result;
	}

	@Override
	public TypedGraph merge(TypedGraph base, TypedGraph... interSources) throws NothingReturnedException {
		TypedGraph result = base.getCopy();
		
		// each typed node n in result, collect images in interSources (null means deleted, Any means default, T means required to be changed to T type)
		//   if all the images of n are compatible, (1) delete or (2) change to the common sub-type
		
		long start,end;
		
		Map<TypedNode, TypedNode> replacedNodes = new HashMap<>();
		
		start = System.currentTimeMillis();
		TypeNode[] nodeImages = new TypeNode[interSources.length];
		for(TypedNode baseNode : base.getAllTypedNodes()) {
			for(int i=0;i<interSources.length;i++) {
				nodeImages[i] = TypedGraph.computeImage(baseNode, base, interSources[i]);
			}
			
			try {
				TypeNode finalType = TypedGraph.computeType(nodeImages, base.getTypeGraph());
				
				if(finalType==TypeNode.NULL_TYPE) {
					result.remove(baseNode);
				} else {
					if(finalType==TypeNode.ANY_TYPE)
						finalType = baseNode.getType();
					
					TypedNode n = new TypedNode();
					n.setType(finalType);
					
					for(TypedGraph image : interSources) {
						n.mergeIndex(image.getElementByIndexObject(baseNode.getIndex()));
					}
					
					result.replaceWith2(baseNode, n);
					replacedNodes.put(baseNode, n);
				}
				
			} catch (NothingReturnedException e) {
				throw e;
			}
		}

		
		replacedNodes.forEach((nr,n)->{
			List<TypedEdge> removedTypedEdges = new ArrayList<TypedEdge>();
			if(nr!=n) {
				TypeNode nodeType = n.getType();
				
				base.getAllTypedEdges().replaceAll(e->{
					TypeEdge edgeType = e.getType();
					TypeNode sourceType = edgeType.getSource();
					TypeNode targetType = edgeType.getTarget();
					
					if(e.getSource()==nr && e.getTarget()==nr) {
						if(base.getTypeGraph().isSuperTypeOf(nodeType, sourceType) && base.getTypeGraph().isSuperTypeOf(nodeType, targetType)) {
							TypedEdge res = new TypedEdge();
							res.setSource(n);
							res.setTarget(n);
							res.setType(edgeType);
							res.mergeIndex(e);
							base.reindexing(res);
							return res;
						} else {
							removedTypedEdges.add(e);
							base.clearIndex(e);
							return e;
						}
					} else if(e.getSource()==nr) {
						if(base.getTypeGraph().isSuperTypeOf(nodeType, sourceType)) {
							TypedEdge res = new TypedEdge();
							res.setSource(n);
							res.setTarget(e.getTarget());
							res.setType(edgeType);
							res.mergeIndex(e);
							base.reindexing(res);
							return res;
						} else {
							removedTypedEdges.add(e);
							base.clearIndex(e);
							return e;
						}
					} else if(e.getTarget()==nr) {
						if(base.getTypeGraph().isSuperTypeOf(nodeType, targetType)) {
							TypedEdge res = new TypedEdge();
							res.setSource(e.getSource());
							res.setTarget(n);
							res.setType(edgeType);
							res.mergeIndex(e);
							base.reindexing(res);
							return res;
						} else {
							removedTypedEdges.add(e);
							base.clearIndex(e);
							return e;
						}
					} else 
						return e;
				});
			}
			base.getAllTypedEdges().removeAll(removedTypedEdges);
		});
		
		replacedNodes.forEach((nr,n)->{
			List<ValueEdge> removedValueEdges = new ArrayList<ValueEdge>();
			if(nr!=n) {
				TypeNode nodeType = n.getType();
				base.getAllValueEdges().replaceAll(e->{
					PropertyEdge edgeType = e.getType();
					TypeNode sourceType = edgeType.getSource();
					
					if(e.getSource()==nr) {
						if(base.getTypeGraph().isSuperTypeOf(nodeType, sourceType)) {
							ValueEdge res = new ValueEdge();
							res.setSource(n);
							res.setTarget(e.getTarget());
							res.setType(edgeType);
							res.mergeIndex(e);
							base.reindexing(res);
							return res;
						} else {
							removedValueEdges.add(e);
							base.clearIndex(e);
							return e;
						}
					} else return e;
				});
				
			}
			base.getAllValueEdges().removeAll(removedValueEdges);
		});
		
		
		end = System.currentTimeMillis();
		System.out.println(end-start);
		
		for(TypedGraph image : interSources) {
			image.getAllValueNodes().forEach(v->{result.addValueNode(v);});
		}
		
	
		
		// each typed edge e in the result, collect images in interSources (null means deleted, Any means default, <s,t>:T means required)
		TypedEdge[] typedEdgeImages = new TypedEdge[interSources.length];
		for(TypedEdge baseEdge : base.getAllTypedEdges()) {
			for(int i=0;i<interSources.length;i++) {
				typedEdgeImages[i] = TypedGraph.computeImage(baseEdge, base, interSources[i]);
			}
			try {
				Tuple3<TypedNode, TypedNode, TypeEdge> finalEdgeInfo = TypedGraph.computeEdge(baseEdge, typedEdgeImages);
				if(finalEdgeInfo==null) {
					result.remove(baseEdge);
				} else {
					TypedNode source = result.getElementByIndexObject(finalEdgeInfo.first.getIndex());
					TypedNode target = result.getElementByIndexObject(finalEdgeInfo.second.getIndex());
					TypedEdge edge = new TypedEdge();
					edge.setSource(source);
					edge.setTarget(target);
					edge.setType(finalEdgeInfo.third);
					
					for(TypedGraph image : interSources) {
						edge.mergeIndex(image.getElementByIndexObject(baseEdge.getIndex()));
					}
					result.replaceWith(baseEdge, edge);
				}
			} catch (NothingReturnedException e) {
				throw e;
			}
		}
		
		// each typed edge e in the result, collect images in interSources (null means deleted, Any means default, <s,t>:T means required)
		ValueEdge[] valueEdgeImages = new ValueEdge[interSources.length];
		for(ValueEdge baseEdge : base.getAllValueEdges()) {
			for(int i=0;i<interSources.length;i++) {
				valueEdgeImages[i] = TypedGraph.computeImage(baseEdge, base, interSources[i]);
			}
			try {
				Tuple3<TypedNode, ValueNode, PropertyEdge> finalEdgeInfo = TypedGraph.computeEdge(baseEdge, valueEdgeImages);
				if(finalEdgeInfo==null) {
					result.remove(baseEdge);
				} else {
					TypedNode source = result.getElementByIndexObject(finalEdgeInfo.first.getIndex());
					ValueNode target = finalEdgeInfo.second;
					ValueEdge edge = new ValueEdge();
					edge.setSource(source);
					edge.setTarget(target);
					edge.setType(finalEdgeInfo.third);
					
					for(TypedGraph image : interSources) {
						edge.mergeIndex(image.getElementByIndexObject(baseEdge.getIndex()));
					}
					result.replaceWith(baseEdge, edge);
				}
			} catch (NothingReturnedException e) {
				throw e;
			}
		}
		
		// add extra nodes and edges
		
		for(TypedGraph image : interSources) {
			for(TypedNode n : image.getAllTypedNodes()) {
				try {
					base.getElementByIndexObject(n.getIndex());
				} catch (NothingReturnedException e) {
					TypedNode rn = null;
					try {
						rn = result.getElementByIndexObject(n.getIndex());
					} catch (NothingReturnedException e1) {
						result.addTypedNode(n);
						rn = null;
					} finally {
						if(rn!=null) {
							TypeNode lt = rn.getType();
							TypeNode rt = n.getType();
							TypeNode ct = base.getTypeGraph().computeSubtype(lt, rt);
							if(ct==TypeNode.NULL_TYPE) {
								// incompatible
								throw new NothingReturnedException();
							} else {
//								TypedNode res = new TypedNode();
//								res.setType(ct);;
//								res.mergeIndices(rn);
//								res.mergeIndices(n);
//								result.replaceWith(rn, res);
								rn.mergeIndex(n);
								rn.setType(ct);
								result.reindexing(rn);
							}
						}
					}
				}
			}
			
			for(TypedEdge e : image.getAllTypedEdges()) {
				try {
					base.getElementByIndexObject(e.getIndex());
				} catch (Exception ex) {
					TypedEdge re = null;
					try {
						re = result.getElementByIndexObject(e.getIndex());
					} catch (Exception ex2) {
						TypedNode source = result.getElementByIndexObject(e.getSource().getIndex());
						TypedNode target = result.getElementByIndexObject(e.getTarget().getIndex());
						if(e.getSource()!=source || e.getTarget()!=target) {
							TypedEdge ne = new TypedEdge();
							ne.setSource(source);
							ne.setTarget(target);
							ne.setType(e.getType());
							ne.mergeIndex(e);
							result.addTypedEdge(ne);
						} else result.addTypedEdge(e);
						re = null;
					} finally {
						if(re!=null) {
							if(re.getType()!=e.getType()
									|| !re.getSource().getIndex().equals(e.getSource().getIndex())
									|| !re.getTarget().getIndex().equals(e.getTarget().getIndex())) {
								throw new NothingReturnedException();
							} else {
								re.mergeIndex(e);
								result.reindexing(re);
							}
						}
					}
				}
			}
			
			for(ValueEdge e : image.getAllValueEdges()) {
				try {
					base.getElementByIndexObject(e.getIndex());
				} catch (Exception ex) {
					ValueEdge re = null;
					try {
						re = result.getElementByIndexObject(e.getIndex());
					} catch (Exception ex2) {
						TypedNode source = result.getElementByIndexObject(e.getSource().getIndex());
//						ValueNode target = e.getTarget();
						if(e.getSource()!=source) {
							ValueEdge ne = new ValueEdge();
							ne.setSource(source);
							ne.setTarget(e.getTarget());
							ne.setType(e.getType());
							ne.mergeIndex(e);
							result.addValueEdge(ne);
						} else result.addValueEdge(e);
//						result.addValueEdge(e);
						re = null;
					} finally {
						if(re!=null) {
							if(re.getType()!=e.getType()
									|| !re.getSource().getIndex().equals(e.getSource().getIndex())
									|| re.getTarget().equals(e.getTarget())==false) {
								throw new NothingReturnedException();
							} else {
								re.mergeIndex(e);
								result.reindexing(re);
							}
						}
					}
				}
			}
		}
		
		OrderInformation[] orders = new OrderInformation[interSources.length];
		for(int i=0;i<interSources.length;i++)
			orders[i] = interSources[i].getOrder();
		result.getOrder().merge(orders);
		
		List<GraphConstraint> cons = new ArrayList<>();
		cons.add(base.getConstraint());
		for(TypedGraph g : interSources) {
			cons.add(g.getConstraint());
		}
		result.setConstraint(GraphConstraint.and(cons));
		// check
		
		return result;
	}

}

package edu.ustb.sei.mde.graph.typedGraph;

import edu.ustb.sei.mde.bxcore.exceptions.NothingReturnedException;

public interface ITypedGraphMerger {
	TypedGraph additiveMerge(TypedGraph host, TypedGraph addition);
	TypedGraph merge(TypedGraph base, TypedGraph... interSources) throws NothingReturnedException;
	
	
	static public final ITypedGraphMerger merger = new ParallelTypedGraphMerger();
}

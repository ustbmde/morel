package edu.ustb.sei.mde.graph.typedGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;

import edu.ustb.sei.mde.bxcore.exceptions.NothingReturnedException;
import edu.ustb.sei.mde.bxcore.structures.Index;
import edu.ustb.sei.mde.structure.PairMap;
import edu.ustb.sei.mde.structure.Tuple2;

public class OrderInformation {
	private Set<Tuple2<Index,Index>> order = new HashSet<>();
	
	public OrderInformation() {
	}
	
	public void add(Index from, Index to) {
		order.add(Tuple2.make(from,to));
		cache.clear();
	}
	
	public void propagation(List<? extends IndexableElement> array) {
		// use floyd algorithm
		for(IndexableElement k : array) {
			for(IndexableElement i : array) {
				if(i==k) continue;
				if(!contains(i.getIndex(), k.getIndex())) continue; // if not i->k, we cannot propagate order via k
				for(IndexableElement j : array) {
					if(i==j || j==k) continue;
					if(contains(i.getIndex(), j.getIndex())) continue; // no need to propagate
					if(contains(k.getIndex(), j.getIndex())) {
						add(i.getIndex(),j.getIndex());
					}
				}
			}
		}
	}
	
	public boolean contains(Index from, Index to) {
		return  order.stream().anyMatch(t->t.first.equals(from) && t.second.equals(to));
	}
	
	
	
	public boolean path(Index from, Index to) {
		Boolean result = null;
		if((result=cache.get(from, to))!=null) return result;
		Set<Index> visited = new HashSet<>();
		return path(from, to, visited);
	}
	
	private PairMap<Index, Index, Boolean> cache = new PairMap<>();
	
	private boolean path(Index from, Index to, Set<Index> visited) {
		if(visited.contains(from)) return false;
		visited.add(from);
		Boolean result = null;
		if((result=cache.get(from, to))!=null) return result;
		result = order.stream().anyMatch(o->o.first.equals(from) && (o.second.equals(to) || path(o.second, to, visited)));
		cache.put(from, to, result);
		return result;
	}
	
	public OrderInformation getCopy() {
		OrderInformation oi = new OrderInformation();
		oi.order.addAll(this.order);
		return oi;
	}
	
	public void addAll(OrderInformation addO) {
		addO.order.forEach(t->{
			add(t.first, t.second);
		});
	}
	
	public void merge(OrderInformation... merges) {
		for(OrderInformation oi : merges) 
			addAll(oi);
	}
	
	public boolean validate() {
		List<Index> indices = order.stream().map(o->o.first).distinct().collect(Collectors.toList());
		try {
			planOrder(indices);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
		
	public Index[] planOrder(List<Index> indices) throws NothingReturnedException {
		Model model = new Model();
		
		Map<Index, IntVar> varMap = new HashMap<>();
		
		indices.forEach(i -> varMap.put(i, model.intVar(0, indices.size()-1)));

		IntVar diff = model.intVar("diff", 0, indices.size()*indices.size());
		IntVar[] diffs = new IntVar[indices.size()];
		{
			int i=0;
			for(i=0;i<indices.size();i++) {
				diffs[i] = model.intAbsView(model.intOffsetView(varMap.get(indices.get(i)), -i));
			}
		}
		
		indices.forEach(f->{
			indices.forEach(t->{
				if(f==t) return;
				if(path(f,t)) {
					model.arithm(varMap.get(f), "<", varMap.get(t)).post();
				}
			});
		});
		
		model.allDifferent(varMap.values().toArray(new IntVar[varMap.size()])).post();
		
		model.sum(diffs, "=", diff).post();
		model.setObjective(false, diff);
		
		if(model.getSolver().solve()) {
			Index[] result = new Index[indices.size()];
			indices.forEach(i->{
				result[varMap.get(i).getValue()] = i;
			});
			return result;
		} else {
			throw new NothingReturnedException("Conflict in orders");
		}
	}
	
	public <K extends IndexableElement> BaseLocalOrder buildLocalOrder(Set<List<K>> groups) {
		return new BaseLocalOrder(order, groups);
	}
	
	public <K extends IndexableElement> LocalOrder buildLocalOrder(Set<List<K>> groups, PositionMap baseMap) {
		return new LocalOrder(order, groups, baseMap);
	}
}

enum OrderKind {
	HARD_LE,
	HARD_GT,
	SOFT_LE,
	SOFT_GT,
	UNK,
	CONFLICT;
	
	public OrderKind base(OrderKind base) {
		OrderKind branch = this;
		if(base==UNK || branch==UNK) return branch;
		if(base==CONFLICT || branch==CONFLICT) return CONFLICT;
		
		switch(branch) {
		case HARD_LE: {
			if(base==HARD_GT) return CONFLICT;
			else return branch;
		}
		case HARD_GT: {
			if(base==HARD_LE) return CONFLICT;
			else return branch;
		}
		case SOFT_LE: {
			if(base==HARD_LE) return HARD_LE;
			else return CONFLICT;
		}
		case SOFT_GT: {
			if(base==HARD_GT) return HARD_GT;
			else return CONFLICT;
		}
		default:
			return CONFLICT;
		}
	}
	
	public boolean isHard() {
		return this==HARD_GT || this==HARD_LE;
	}
	public boolean isSoft() {
		return !isHard() && this!=CONFLICT;
	}
	
	public OrderKind comp(OrderKind right) {
		boolean leftIsHard = this.isHard();
		boolean rightIsSoft = right.isSoft();
		boolean rightIsHard = right.isHard();
		boolean leftIsSoft = this.isSoft();

		if(leftIsHard && rightIsSoft) return this;
		if(rightIsHard && leftIsSoft) return right;
		if(leftIsHard && rightIsHard) {
			if(this==right) return this;
			else return CONFLICT;
		}
		if(leftIsSoft && rightIsSoft) {
			if(this==right) return this;
			else if(this==UNK) return right;
			else if(right==UNK) return this;
			else return CONFLICT; // not reachable
		}
		
		return CONFLICT;
	}

	OrderKind reverse() {
		switch(this) {
		case UNK: return UNK;
		case CONFLICT: return CONFLICT;
		case HARD_GT: return HARD_LE;
		case HARD_LE: return HARD_GT;
		case SOFT_GT: return SOFT_LE;
		case SOFT_LE: return SOFT_GT;
		}
		return null;
	}
}

class PositionMap
{
	private Map<Object, Integer> positionMap;
	public <K extends IndexableElement> PositionMap(Set<List<K>> actualOrders)
	{
		positionMap = new HashMap<>();
		actualOrders.forEach(actualOrder->{
			for(int i=0;i<actualOrder.size();i++) {
				Index idx = actualOrder.get(i).getIndex();
				for(Object id : idx.internalIndices()) {
					positionMap.put(id, i);								
				}
			}
		});
	}
	
	public int testPosition(Index idx) {
		Integer p = null;
		for(Object id : idx.internalIndices()) {
			if((p=positionMap.get(id))==null) continue;
			return p;
		}
		return -1;
	}
}

class BaseLocalOrder {
	public <K extends IndexableElement> BaseLocalOrder(Set<Tuple2<Index, Index>> hardOrder, Set<List<K>> actualOrders) {
		buildHardOrders(hardOrder);
		positionMap = new PositionMap(actualOrders);
	}
	
	public PositionMap positionMap;
	
	private Set<Tuple2<Object, Object>> hardOrders; // orders enforced by explicit constraints
	private static void expandTuple(Index l, Index r, Set<Tuple2<Object, Object>> res) {
		for(Object li : l.internalIndices()) {
			for(Object ri : r.internalIndices()) {
				res.add(Tuple2.make(li, ri));
			}
		}
	}
	private void buildHardOrders(Set<Tuple2<Index, Index>> hardOrder)
	{
		hardOrders = new HashSet<>();
		hardOrder.forEach(pair->{
			expandTuple(pair.first, pair.second, hardOrders);
		});
	}
	
	private OrderKind testOrder(IndexableElement left, IndexableElement right) {
		assert left!=right;
		
		Index f = left.getIndex();
		Index t = right.getIndex();
		
		if(containedInHard(f,t)) return OrderKind.HARD_LE;
		if(containedInHard(t,f)) return OrderKind.HARD_GT;

		return OrderKind.UNK;
	}
	
	private boolean containedInHard(Index from, Index to) {
		for(Object fi : from.internalIndices()) {
			for(Object ti : to.internalIndices()) {
				if(hardOrders.contains(Tuple2.make(fi, ti))) return true;
			}
		}
		return false;
	}
	
	
	public <K extends IndexableElement> OrderKind checkOrder(K a, K b, LocalOrder[] branchOrders) {
		if(a==b) return OrderKind.UNK;
		OrderKind order = OrderKind.UNK;
		final OrderKind bo = testOrder(a, b);
		for(LocalOrder branchOrder : branchOrders) {
			order = order.comp(branchOrder.testOrder(a, b).base(bo));
		}
		if(order!=OrderKind.CONFLICT) return order;
		else throw new NothingReturnedException("Conflict in orders");
	}
}


class LocalOrder {
	private Set<Tuple2<Object, Object>> hardOrders; // orders enforced by explicit constraints
	private Set<Tuple2<Object, Object>> softOrders; // orders enforced by actual orders
	
//	private Map<Object, Integer> positionMap;
	
	private static void expandTuple(Index l, Index r, Set<Tuple2<Object, Object>> res) {
		for(Object li : l.internalIndices()) {
			for(Object ri : r.internalIndices()) {
				res.add(Tuple2.make(li, ri));
			}
		}
	}
	private void buildHardOrders(Set<Tuple2<Index, Index>> hardOrder)
	{
		hardOrders = new HashSet<>();
		hardOrder.forEach(pair->{
			expandTuple(pair.first, pair.second, hardOrders);
		});
	}
	
	private <K extends IndexableElement> void buildSoftOrders(Set<List<K>> actualOrders, PositionMap basePositionMap)
	{
		softOrders = new HashSet<>();
		actualOrders.forEach(group->{
			buildSoftOrders(softOrders, group, basePositionMap);
		});
	}
	
	// the basic idea is to compute the inverse orders
	// worse case is O(n^2). the best case is O(nlog(n))
	private <K extends IndexableElement> List<K> buildSoftOrders(Set<Tuple2<Object, Object>> orders, List<K> actualOrders, PositionMap basePositionMap)
	{
		if(actualOrders.size()==1) return actualOrders;
		else {
			int mid = actualOrders.size()/2;
			List<K> left = actualOrders.subList(0, mid);
			List<K> right = actualOrders.subList(mid, actualOrders.size());
			
			List<K> reorderedLeft = buildSoftOrders(orders, left, basePositionMap);
			List<K> reorderedRight = buildSoftOrders(orders, right, basePositionMap);
			
			// merge
			List<K> result = new ArrayList<>(actualOrders.size());
			int li = 0, ri = 0;
			while(li<reorderedLeft.size() && ri < reorderedRight.size()) {
				K l = reorderedLeft.get(li);
				K r = reorderedRight.get(ri);
				
				int lbp = basePositionMap.testPosition(l.getIndex());
				int rbp = basePositionMap.testPosition(r.getIndex());
				
				if(lbp==-1 || rbp==-1) {
					if(lbp!=-1) {
						result.add(l);
						li++;
					} else if(rbp!=-1) {
						result.add(r);
						ri++;
					} else break;
				} else if(lbp > rbp) { // different from base
					result.add(r);
					ri++;
					expandTuple(l.getIndex(), r.getIndex(), orders);
				} else {
					result.add(l);
					li++;
				}
			}
			
			// handle left remainder
			while(li < reorderedLeft.size()) {
				K l = reorderedLeft.get(li);
				int lbp = basePositionMap.testPosition(l.getIndex());
				if(lbp==-1) {
					break;
				} else {
					result.add(l);
					li++;
				}
			}
			
			while(ri < reorderedRight.size()) {
				K r = reorderedRight.get(ri);
				int rbp = basePositionMap.testPosition(r.getIndex());
				if(rbp==-1) {
					break;
				} else {
					result.add(r);
					ri++;
				}
			}
			
			while(li < reorderedLeft.size()) {
				K l = reorderedLeft.get(li);
				result.add(l);
				for(K r : right) {
					expandTuple(l.getIndex(), r.getIndex(), orders);
				}
				li ++;
			}
			
			while(ri < reorderedRight.size()) {
				K r = reorderedRight.get(li);
				result.add(r);
				for(K l : left) {
					expandTuple(l.getIndex(), r.getIndex(), orders);
				}
				ri ++;
			}
			
			if(result.size()!=actualOrders.size()) throw new RuntimeException("losing elements");
			return result;
		}
	}
	
	
	
	public <K extends IndexableElement> LocalOrder(Set<Tuple2<Index, Index>> hardOrder, Set<List<K>> actualOrders, PositionMap basePositionMap) {
		super();
		buildHardOrders(hardOrder);
		buildSoftOrders(actualOrders, basePositionMap);
		
	}

	private boolean containedInHard(Index from, Index to) {
		for(Object fi : from.internalIndices()) {
			for(Object ti : to.internalIndices()) {
				if(hardOrders.contains(Tuple2.make(fi, ti))) return true;
			}
		}
		return false;
	}
	
	private boolean containedInSoft(Index from, Index to) {
		for(Object fi : from.internalIndices()) {
			for(Object ti : to.internalIndices()) {
				if(softOrders.contains(Tuple2.make(fi, ti))) return true;
			}
		}
		return false;
	}

	
	public OrderKind testOrder(IndexableElement left, IndexableElement right) {
		assert left!=right;
		
		Index f = left.getIndex();
		Index t = right.getIndex();
		
		if(containedInHard(f,t)) return OrderKind.HARD_LE;
		if(containedInHard(t,f)) return OrderKind.HARD_GT;
		
		if(containedInSoft(f,t)) return OrderKind.SOFT_LE;
		if(containedInSoft(t,f)) return OrderKind.SOFT_GT;

		return OrderKind.UNK;
	}
}
